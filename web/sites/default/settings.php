<?php

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/**
 * Include the Pantheon-specific settings file.
 *
 * n.b. The settings.pantheon.php file makes some changes
 *      that affect all envrionments that this site
 *      exists in.  Always include this file, even in
 *      a local development environment, to insure that
 *      the site settings remain consistent.
 */
include_once __DIR__ . "/settings.pantheon.php";

/**
 * Asymm specific settings.
 */

// Change it after install!
if (!defined('ASYMM_SITE_SHORT_NAME')) {
  define('ASYMM_SITE_SHORT_NAME', 'asymm_upstream');
}

/**
 * Move config sync directory outside the webroot.
 * We don't need any other confif dir, because environment based condigurations
 * are handled by config_split module.
 */
$config_directories['sync'] = dirname(DRUPAL_ROOT) . '/config/sync';


/**
 * Private file path:
 *
 * A local file system path where private files will be stored. This directory
 * must be absolute, outside of the Drupal installation directory and not
 * accessible over the web.
 *
 * Note: Caches need to be cleared when this value is changed to make the
 * private:// stream wrapper available to the system.
 *
 * See https://www.drupal.org/documentation/modules/file for more information
 * about securing private files.
 */
$settings['file_private_path'] = dirname(DRUPAL_ROOT) . '/private-files';

/**
 * Set up environment indicator colors. @todo: Figure out the proper colors.
 * If you use your own settings.local.php, you can override them.
 */

$config['environment_indicator.indicator']['bg_color'] = '#ffffff';
$config['environment_indicator.indicator']['fg_color'] = '#000000';
$config['environment_indicator.indicator']['name'] = 'local';

if (isset($_ENV['PANTHEON_ENVIRONMENT'])) {
  switch ($_ENV['PANTHEON_ENVIRONMENT']) {
    case 'dev':
      $config['environment_indicator.indicator']['bg_color'] = '#008b49';
      $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
      $config['environment_indicator.indicator']['name'] = 'dev';
      break;
    case 'test':
      $config['environment_indicator.indicator']['bg_color'] = '#ddffdd';
      $config['environment_indicator.indicator']['fg_color'] = '#000000';
      $config['environment_indicator.indicator']['name'] = 'test';
      break;
    case 'live':
      $config['environment_indicator.indicator']['bg_color'] = '#da251c';
      $config['environment_indicator.indicator']['fg_color'] = '#ffffff';
      $config['environment_indicator.indicator']['name'] = 'live';
      break;
    default:
      $config['environment_indicator.indicator']['bg_color'] = '#ffffff';
      $config['environment_indicator.indicator']['fg_color'] = '#000000';
      $config['environment_indicator.indicator']['name'] = 'Unrecognized environment!';
      break;
  }
}

/**
 * If there is a local settings file, then include it.
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include_once $local_settings;
}

$settings['install_profile'] = 'asymm_upstream';

$settings['hash_salt'] = 'gWKMKqG5eAvRE9hRMuTl1bbRJKMgMAhgt7JKS3HkSc1HsRdpF4UF9fQmH8W7ZLR0ntSlZ-LljQ';
