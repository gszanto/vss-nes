<?php

/**
 * @file
 * Functions to support theming in the Asymm Upstream Base Theme.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_system_theme_settings_alter() for settings form.
 *
 * Replace Barrio setting options with subtheme ones.
 */
function asymm_upstream_base_theme_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  $form['components']['navbar']['bootstrap_barrio_navbar_top_background']['#options'] = array(
      'bg-primary' => t('Primary'),
      'bg-secondary' => t('Secondary'),
      'bg-light' => t('Light'),
      'bg-dark' => t('Dark'),
      'bg-white' => t('White'),
      'bg-transparent' => t('Transparent'),
  );
  $form['components']['navbar']['bootstrap_barrio_navbar_background']['#options'] = array(
      'bg-primary' => t('Primary'),
      'bg-secondary' => t('Secondary'),
      'bg-light' => t('Light'),
      'bg-dark' => t('Dark'),
      'bg-white' => t('White'),
      'bg-transparent' => t('Transparent'),
  );
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * This hook for adding a custom suggestion page template.
 * Now you can use the template page--[content-type].html.twig
 */
function asymm_upstream_base_theme_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    $content_type = $node->bundle();
    $suggestions[] = 'page__' . $content_type;
  }
}
